﻿using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AssessmentTests.Pages;

public class SwagLabsDemoPage
{
    private readonly 
        By UsernameEntry = 
        By.XPath("//*[@id=\"user-name\"]");

    private readonly 
        By PasswordEntry = 
        By.XPath("//*[@id=\"password\"]");

    private readonly 
        By LogInButton = 
        By.XPath("//*[@id=\"login-button\"]");

    private readonly 
        By ErorMessageText = 
        By.XPath("//*[@id=\"login_button_container\"]/div/form/div[3]/h3");

    private readonly 
        By Title = 
        By.XPath("//*[@id=\"header_container\"]/div[1]/div[2]/div");

    private readonly WebDriverWait _waiter;
    protected readonly IWebDriver _driver;
    private static readonly Logger logger = LogManager.GetCurrentClassLogger();

    public SwagLabsDemoPage(IWebDriver driver)
    {
        _driver = driver;
        _waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
    }

    public void InputDataInFields(string username, string password)
    {
        try
        {
            _waiter.Until(d => d.FindElement(UsernameEntry)).SendKeys(username);
            logger.Info("Username entered");
            _waiter.Until(d => d.FindElement(PasswordEntry)).SendKeys(password);
            logger.Info("Password entered");


        }
        catch (Exception e)
        {
            logger.Error(e.Message);
        }
        
    }

    public void ClearUsernameField()
    {
        try
        {
            Thread.Sleep(1000);
            _waiter.Until(d => d.FindElement(UsernameEntry)).SendKeys(Keys.Control + "A" + Keys.Backspace);
            logger.Info("The username field has been cleared");
        }
        catch (Exception e)
        {
            logger.Error(e.Message);
        }
        

    }

    public void ClearPasswordField()
    {
        try
        {
            Thread.Sleep(1000);
            _waiter.Until(d => d.FindElement(PasswordEntry)).SendKeys(Keys.Control + "A" + Keys.Backspace);
            logger.Info("The password field has been cleared");
        }
        catch (Exception e)
        { 
            logger.Error(e.Message);
        }
        

    }

    public void LogInButtonClick()
    {
        try
        {
            _waiter.Until(d => d.FindElement(LogInButton)).Click();
            logger.Info("The login button has been clicked");
        }
        catch (Exception e)
        {
            logger.Error(e.Message);
        }
        
    }

    public string GetErrorMessage()
    {
        try
        {
            string errorText = _waiter.Until(d => d.FindElement(ErorMessageText)).Text;
            return errorText;
        }
        catch (Exception e)
        {
            logger.Error(e.Message);
            return String.Empty;
        }
        
    }

    public string GetTitle()
    {
        try
        {
            string title = _waiter.Until(d => d.FindElement(Title)).Text;
            return title;
        }
        catch (Exception e)
        {
            logger.Error(e.Message);
            return String.Empty;
        }
    }


}
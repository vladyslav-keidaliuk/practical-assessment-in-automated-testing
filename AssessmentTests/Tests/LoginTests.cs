using AssessmentTests.Pages;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Client;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using ILogger = NUnit.Framework.Internal.ILogger;

namespace AssessmentTests.Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.Children)]
    public class Tests
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private static IEnumerable<object[]> TestCredentials()
        {
            yield return new object[] { "test", "test" };
        }

        private static IEnumerable<object[]> ValidCredentials()
        {
            yield return new object[] { "standard_user", "secret_sauce" };
        }


        [SetUp]
        public void Setup()
        {
            logger.Info("Tests started");
        }

        [Test]
        [TestCaseSource(nameof(TestCredentials))]
        public void LoginWithEmptyAllCredentials(string username, string password)
        {
            try
            {
                logger.Info("Test LoginWithEmptyAllCredentials starting..");
                WebDriver driver = new ChromeDriver();
                driver.Manage().Window.Maximize();
                SwagLabsDemoPage demoPage = new SwagLabsDemoPage(driver);
                driver.Url = "https://www.saucedemo.com/";

                demoPage.InputDataInFields(username, password);
                demoPage.ClearUsernameField();
                demoPage.ClearPasswordField();
                demoPage.LogInButtonClick();
                string errorText = demoPage.GetErrorMessage();

                driver.Quit();
                logger.Info("Driver for LoginWithEmptyAllCredentials test closed ");
                Assert.IsTrue(errorText.Equals("Epic sadface: Username is required"));
                logger.Info("Test LoginWithEmptyAllCredentials stoped.");
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }
        }


        [Test]
        [TestCaseSource(nameof(TestCredentials))]
        public void LoginWithEmptyPasswordField(string username, string password)
        {
            try
            {
                logger.Info("Test LoginWithEmptyPasswordField starting..");
                WebDriver driver = new ChromeDriver();
                driver.Manage().Window.Maximize();
                SwagLabsDemoPage demoPage = new SwagLabsDemoPage(driver);

                driver.Url = "https://www.saucedemo.com/";

                demoPage.InputDataInFields(username, password);
                demoPage.ClearPasswordField();
                demoPage.LogInButtonClick();

                string errorText = demoPage.GetErrorMessage();

                driver.Quit();
                logger.Info("Driver for LoginWithEmptyPasswordField test closed ");
                Assert.IsTrue(errorText.Equals("Epic sadface: Password is required"));
                logger.Info("Test LoginWithEmptyPasswordField stoped.");
            }
            catch (Exception e)
            {
               logger.Error(e.Message);
            }
        }


        [Test]
        [TestCaseSource(nameof(ValidCredentials))]
        public void LoginWithCorrectCredentials(string username, string password)
        {
            logger.Info("Test LoginWithCorrectCredentials starting..");
            WebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            SwagLabsDemoPage demoPage = new SwagLabsDemoPage(driver);

            driver.Url = "https://www.saucedemo.com/";
            

            demoPage.InputDataInFields(username, password);
            demoPage.LogInButtonClick();

            string title = demoPage.GetTitle();

            driver.Quit();
            logger.Info("Driver for  LoginWithCorrectCredentials test closed ");
            Assert.IsTrue(title.Equals("Swag Labs"));
            logger.Info("Test LoginWithCorrectCredentials stoped.");

        }
    }
}